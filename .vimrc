set nocompatible

set enc=utf-8
set fenc=utf-8
set termencoding=utf-8
set autoindent
set smartindent

set nowrap
set tabstop=8
set softtabstop=8
set shiftwidth=8
set noexpandtab

set t_Co=256
syntax on

set number
set showmatch
set comments=sl:/*,mb:\ *,elx:\ */

set hlsearch
set incsearch
set ignorecase
set smartcase

set colorcolumn=80

autocmd BufWritePre * :%s/\s\+$//e
