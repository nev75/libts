all:
	$(MAKE) all -C ./src

debug:
	$(MAKE) debug -C ./src

release:
	$(MAKE) release -C ./src

remake:
	$(MAKE) remake -C ./src

prep:
	$(MAKE) prep -C ./src

clean:
	$(MAKE) clean -C ./src
